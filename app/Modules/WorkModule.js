const AbstractApiHelper = require('../Helpers/AbstractApiHelper');
const CompanyRepository = require('../Repositories/CompanyRepository');
const CompanyModule = require('../Modules/CompanyModule');

class WorkModule {

    get getType() {
        return 'work';
    }

    get getRelations() {
        const companyModule = new CompanyModule();

        return { company: companyModule };
    }

    assignValues(data) {
        const abstractHelper = new AbstractApiHelper();
        const companyRepository = new CompanyRepository();
        const companyModule = new CompanyModule();

        data = {
            id: data.id ? data.id : '',
            job: data.job ? data.job : '',
            company: data.company ? abstractHelper.assignValuesObject(data.company, companyRepository, companyModule) : '',
        };

        return data;
    }

}

module.exports = WorkModule;
