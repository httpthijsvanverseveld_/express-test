class AddressModule {

    get getType() {
        return 'address';
    }

    get getRelations() {
        return {};
    }

    assignValues(data) {
        data = {
            id: data.id ? data.id : '',
            street: data.street ? data.street : '',
            zipcode: data.zipcode ? data.zipcode : '',
            number: data.number ? data.number : '',
            city: data.city ? data.city : '',
        };

        return data;
    }

}

module.exports = AddressModule;
