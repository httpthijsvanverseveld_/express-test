const AbstractApiHelper = require('../Helpers/AbstractApiHelper');
const AddressModule = require('../Modules/AddressModule');
const AddressRepository = require('../Repositories/AddressRepository');

class CompanyModule {

    get getType() {
        return 'company';
    }

    get getRelations() {
        const addressModule = new AddressModule();
        return { address: addressModule };
    }

    assignValues(data) {
        const abstractHelper = new AbstractApiHelper();
        const addressRepository = new AddressRepository();
        const addressModule = new AddressModule();

        data = {
            id: data.id ? data.id : '',
            name: data.name ? data.name : '',
            address: data.address ? abstractHelper.assignValuesObject(data.address, addressRepository, addressModule) : '',
        };

        return data;
    }

}

module.exports = CompanyModule;
