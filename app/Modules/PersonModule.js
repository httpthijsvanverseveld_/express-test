const AbstractApiHelper = require('../Helpers/AbstractApiHelper');
const AddressModule = require('./AddressModule');
const AddressRepository = require('../Repositories/AddressRepository');
const WorkRepository = require('../Repositories/WorkRepository');
const WorkModule = require('../Modules/WorkModule');

class PersonModule {

    get getType() {
        return 'person';
    }

    get getRelations() {
        const addressModule = new AddressModule();
        const workModule = new WorkModule();

        return { address: addressModule, work: workModule};
    }

    assignValues(data) {
        const abstractHelper = new AbstractApiHelper();
        const addressModule = new AddressModule();
        const addressRepository = new AddressRepository();
        const workRepository = new WorkRepository();
        const workModule = new WorkModule();

        data = {
            id: data.id ? data.id : '',
            firstName: data.firstName ? data.firstName : '',
            lastName: data.lastName ? data.lastName : '',
            address: data.addressId ? abstractHelper.assignValuesObject(data.addressId, addressRepository, addressModule) : '',
            work: data.workId ? abstractHelper.assignValuesObject(data.workId, workRepository, workModule): '',
        };

        return data;
    }

}

module.exports = PersonModule;
