require('dotenv').config();

class CompanyRepository {

    constructor() {
        if (process.env.APP_ENV == 'develop') {
            this._data = [
                {
                    id: 1,
                    name: 'Triple',
                    address: 2
                },
                {
                    id: 2,
                    name: 'Cleaning',
                    address: 1
                },
                {
                    id: 3,
                    name: 'Ramenlappers BV',
                    address: 3
                },
            ];
        }
    }

    findAll() {
        return this._data;
    }

    findById(id) {
        let output = '';

        this._data.forEach((element) => {
            if (element.id == id) {
                output = element;
            }
        });

        return output;
    }

}

module.exports = CompanyRepository;