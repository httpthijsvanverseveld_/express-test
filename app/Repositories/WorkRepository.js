require('dotenv').config();

class WorkRepository {

    constructor() {
        if (process.env.APP_ENV == 'develop') {
            this._data = [
                {
                    id: 1,
                    job: 'programmer',
                    company: 1,
                },
                {
                    id: 2,
                    job: 'cleaner',
                    company: 2,
                },
                {
                    id: 3,
                    job: 'windowcleaner',
                    company: 3,
                },
            ];
        }
    }

    findAll() {
        return this._data;
    }

    findById(id) {
        let output = '';

        this._data.forEach((element) => {
            if (element.id == id) {
                output = element;
            }
        });

        return output;
    }

}

module.exports = WorkRepository;
