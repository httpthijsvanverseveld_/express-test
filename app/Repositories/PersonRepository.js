require('dotenv').config();

class PersonRepository {

    constructor() {
        if (process.env.APP_ENV == 'develop') {
            //Setting default data for testing purposes only
            this._data = [
                {
                    id: 1,
                    firstName: 'Thijs',
                    lastName: 'van Verseveld',
                    addressId: 1,
                    workId: 3,
                },
                {
                    id: 2,
                    firstName: 'Ivo',
                    lastName: 'Toby',
                    addressId: 2,
                    workId: 2,
                },
                {
                    id: 3,
                    firstName: 'Danitja',
                    lastName: 'Vermeer',
                    addressId: 1,
                    workId: 1,
                }
            ];
        }
    }

    findAll() {
        return this._data;
    }

    findById(id) {
        let output = '';

        this._data.forEach((element) => {
            if(element.id == id) {
                output = element;
            }
        });

        return output;
    }

    findByAddress(id) {
        let output = [];

        this._data.forEach((element) => {
            if (element.addressId == id) {
                output.push(element);
            }
        });

        return output;
    }

}

module.exports = PersonRepository;