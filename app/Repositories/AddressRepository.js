require('dotenv').config();

class AddressRepository {

    constructor() {
        if(process.env.APP_ENV == 'develop') {
            this._data = [
                {
                    id: 1,
                    street: 'Straatweg',
                    zipcode: '1234AB',
                    number: 1,
                    city: 'Alkmaar',
                },
                {
                    id: 2,
                    street: 'Wegstraat',
                    zipcode: '2143DB',
                    number: 2,
                    city: 'Alkmaar',
                },
                {
                    id: 3,
                    street: 'Laanweg',
                    zipcode: '1243CB',
                    number: 3,
                    city: 'Alkmaar',
                },
                {
                    id: 4,
                    street: 'Weglaan',
                    zipcode: '2134BA',
                    number: 4,
                    city: 'Alkmaar',
                },
            ];
        }
    }

    findAll() {
        return this._data;
    }

    findById(id) {
        let output = '';

        this._data.forEach((element) => {
            if(element.id == id) {
                output = element;
            }
        });

        return output;
    }

}

module.exports = AddressRepository;
