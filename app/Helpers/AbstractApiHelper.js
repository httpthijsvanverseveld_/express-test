class AbstractApiHelper {

    assignValuesObject(item, Repository, Module) {
        return Module.assignValues(Repository.findById(item));
    }

    /**
     * This is how the relations array should look like:
     * {
     *  'foo': FooModule,
     *  'bar': BarModule,
     * }
     * 
     * Each Relation must have a name and a DataModel
     */
    convertToJsonApi(data, relations, type, meta = {}) {

        let output = {
            data: [],
            included: [],
            meta: meta
        };

        let tempIncluded = [];

        if (!Array.isArray(data)) {
            data = [data];
        }

        /**
         * Foreach {Item} inside {Data}, loop through converter
         */
        data.forEach((item) => {

            let temp = {
                type: type,
                id: item.id,
                attributes: {},
                relationships: {}
            };

            /**
             * Set attributes + relationships + create tempIncluded
             */
            Object.keys(item).forEach((itemKey) => {
                /**
                 * itemValue = The Item
                 */
                const itemValue = item[itemKey];
                if (itemKey != 'id') {
                    temp.attributes[itemKey] = itemValue;
                }

                /**
                 * Check for Relations
                 */
                Object.keys(relations).forEach((relationKey) => {
                    /**
                     * relationValue = The Relation
                     */
                    const relationValue = relations[relationKey];

                    /**
                     * Get all subRelations
                     */
                    let subRelations = { type: relationValue.getType, module: relationValue.getRelations };

                    /**
                     * Check if there is indeed a field called {relationKey}
                     */
                    if (itemKey == relationKey) {

                        /**
                         * Make a Temporary relation
                         */
                        let tempRelation = {
                            data: {
                                type: itemKey,
                                id: itemValue.id
                            }
                        };

                        /**
                         * Make a Temporary include
                         */
                        let tempInclude = {
                            type: itemKey,
                            id: itemValue.id,
                            attributes: {},
                        };

                        /**
                         * Loop through all the fields inside {itemValue}
                         */
                        Object.keys(itemValue).forEach((itemValueKey) => {
                            /**
                             * valueKey = each field inside The Item
                             */
                            const valueKey = itemValue[itemValueKey];

                            /**
                             * Set everything inside of tempInclude.attributes except for {id}
                             */
                            if (itemValueKey != 'id') {
                                tempInclude.attributes[itemValueKey] = valueKey;
                            }

                            Object.keys(subRelations.module).forEach((subRelationKey) => {
                                /**
                                 * subRelationValue = The given Module for the relation
                                 */
                                const subRelationValue = subRelations.module[subRelationKey];

                                /**
                                 * Check if subRelation field is equal to the current itemValue field
                                 */
                                if (subRelationKey == itemValueKey) {

                                    /**
                                     * Make sure tempInclude doens't have all the values in it.. Only the corresponding ID
                                     */
                                    tempInclude.attributes[itemValueKey] = valueKey.id;

                                    /**
                                     * Make a Temporary SubInclude so I can store new things here without overriding {tempInclude}
                                     */
                                    let tempSubInclude = {
                                        type: subRelationKey,
                                        id: valueKey.id,
                                        attributes: {}
                                    };

                                    /**
                                     * Check if {tempInclude.relationships} exist so it can't give back a 'undefined' error
                                     */
                                    if (!tempInclude.relationships) {
                                        tempInclude.relationships = {};
                                    }
                                    tempInclude.relationships[itemValueKey] = { type: itemValueKey, id: valueKey.id };

                                    /**
                                     * Loop through every field inside the subRelation
                                     */
                                    Object.keys(valueKey).forEach((subIncludeKey) => {
                                        /**
                                         * subIncludeValue = The current value
                                         */
                                        const subIncludeValue = valueKey[subIncludeKey];

                                        /**
                                         * Set everything inside of tempSubInclude.attributes except for {id}
                                         */
                                        if (subIncludeKey != 'id') {
                                            tempSubInclude.attributes[subIncludeKey] = subIncludeValue;
                                        }

                                    });

                                    /**
                                     * Push it to the main tempIncluded so everything can be added into the output later on
                                     */

                                    tempIncluded.push(tempSubInclude);
                                }
                            });
                        });

                        tempIncluded.push(tempInclude);
                        temp.relationships[tempRelation.data.type] = tempRelation;
                        if (temp.attributes[relationKey]) {
                            delete temp.attributes[relationKey];
                        }
                    }

                });
            });

            /**
             * Push the info into the output
             */
            output.data.push(temp);

        });

        tempIncluded.forEach((include) => {
            let inside = false;

            output.included.forEach((realInclude) => {
                if (include.type === realInclude.type) {
                    if (include.id === realInclude.id) {
                        inside = true;
                    }
                }
            });

            if (inside == false) {
                output.included.push(include);
            }
        })


        return output;
    }

}

module.exports = AbstractApiHelper;
