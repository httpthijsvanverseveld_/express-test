const AddressRepository = require('../../Repositories/AddressRepository');
const PersonRepository = require('../../Repositories/PersonRepository');
const AddressModule = require('../../Modules/AddressModule');
const PersonModule = require('../../Modules/PersonModule');
const AbstractApiHelper = require('../../Helpers/AbstractApiHelper');

class AddressController {

    index(req, res) {
        const addressRepository = new AddressRepository();
        const addressModule = new AddressModule();
        const abstractApiHelper = new AbstractApiHelper();

        let data = addressRepository.findAll();

        Object.keys(data).forEach((key) => {
            const element = data[key];

            data[key] = addressModule.assignValues(element);
        });

        res.send(abstractApiHelper.convertToJsonApi(data, {}, 'address'));
    }

    specific(req, res) {
        const addressRepository = new AddressRepository();
        const addressModule = new AddressModule();
        const abstractApiHelper = new AbstractApiHelper();

        let data = addressRepository.findById(req.params.id);

        data = addressModule.assignValues(data);

        res.send(abstractApiHelper.convertToJsonApi(data, {}, 'address'));
    }

    residents(req, res) {
        const personRepository = new PersonRepository();
        const personModule = new PersonModule();
        const addressModule = new AddressModule();
        const abstractApiHelper = new AbstractApiHelper();

        let data = personRepository.findByAddress(req.params.id);

        Object.keys(data).forEach((key) => {
            const element = data[key];

            data[key] = personModule.assignValues(element);
        });

        res.send(abstractApiHelper.convertToJsonApi(data, {'address': addressModule}, 'person'));
    }

}

module.exports = AddressController;
