const PersonRepository = require('../../Repositories/PersonRepository');
const PersonModule = require('../../Modules/PersonModule');
const AbstractApiHelper = require('../../Helpers/AbstractApiHelper');
const AddressModule = require('../../Modules/AddressModule');
const CompanyModule = require('../../Modules/CompanyModule');
const WorkModule = require('../../Modules/WorkModule');


class PersonController {
    index(req, res) {
        const personRepository = new PersonRepository();
        const abstractApiHelper = new AbstractApiHelper();
        const personModule = new PersonModule();
        const addressModule = new AddressModule();
        const companyModule = new CompanyModule();
        const workModule = new WorkModule();

        let data = personRepository.findAll();

        Object.keys(data).forEach((key) => {
            const element = data[key];

            data[key] = personModule.assignValues(element);
        });

        res.send(abstractApiHelper.convertToJsonApi(data, { 'address': addressModule, 'work': workModule, }, 'person'));
    }

    specific(req, res) {
        const personRepository = new PersonRepository();
        const abstractApiHelper = new AbstractApiHelper();
        const personModule = new PersonModule();
        const addressModule = new AddressModule();

        let data = personRepository.findById(req.params.id);

        data = personModule.assignValues(data);

        res.send(abstractApiHelper.convertToJsonApi(data, { 'address': addressModule }, 'person'));
    }

}

module.exports = PersonController;