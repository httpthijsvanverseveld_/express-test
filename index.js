const express = require('express');
const app = express();

let PersonController = require('./app/Http/Controllers/PersonController');
PersonController = new PersonController();

let AddressController = require('./app/Http/Controllers/AddressController');
AddressController = new AddressController();

app.get('/api/v1/persons', PersonController.index);
app.get('/api/v1/persons/:id', PersonController.specific);

app.get('/api/v1/address', AddressController.index);
app.get('/api/v1/address/:id', AddressController.specific);
app.get('/api/v1/address/:id/residents', AddressController.residents);

app.listen(3000);
