# README express-test

## API CALLS
For this project there are multiple API calls you can make:

- /api/v1/persons 
- /api/v1/persons/:id 
- /api/v1/address 
- /api/v1/address/:id 
- /api/v1/address/:id/residents 

## MEANINGS OF ACTIONS

### /api/v1/persons
Get all persons.

### /api/v1/persons/:id
Get specific person by his/her id.

### /api/v1/address
Get all Addresses.

### /api/v1/address/:id
Get specific address by it's id.

### /api/v1/address/:id/residents
Get all persons who life on a specific address by it's id.

## INSTALLING
Just run `npm i` or `npm install` and don't forget to make the .env file.
